package com.axis.system.gerrit.plugins.mean;

import static com.google.gerrit.server.config.ConfigResource.CONFIG_KIND;
import static com.google.gerrit.server.project.ProjectResource.PROJECT_KIND;

import com.google.gerrit.extensions.registration.DynamicSet;
import com.google.gerrit.extensions.restapi.RestApiModule;
import com.google.gerrit.extensions.webui.JavaScriptPlugin;
import com.google.gerrit.extensions.webui.WebUiPlugin;

public class MeanModule extends RestApiModule {

  @Override
  protected void configure() {
    DynamicSet.bind(binder(), WebUiPlugin.class)
        .toInstance(new JavaScriptPlugin("feedback-button.html"));

    get(CONFIG_KIND, "meanhost").to(GetMeanHost.class);
    get(PROJECT_KIND, "meanenabled").to(GetMeanEnabled.class);
    get(PROJECT_KIND, "meananalyzers").to(GetMeanAnalyzers.class);
  }
}
