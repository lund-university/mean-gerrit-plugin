(function() {
  'use strict';

  /** Client for Mean API. */
  class MeanClient {
    constructor(url) {
      this.url = url;
    }

    /**
     * Send a "not useful" Feedback message to the Mean server.
     *
     * @param {RobotCommentInfo} comment The comment to send feedback for.
     * @param {AccountInfo} accountInfo Info about user that send feedback.
     * @return {Promise} Resolves if successful, or rejects with error.
     */
    async reportNotUseful(comment, accountInfo) {
      if (!this.url) {
        throw Error('Cannot send report, Mean host not set.');
      }
      if (!comment.properties || !comment.properties.note_id ||
          !comment.properties.analyzer_name || !comment.properties.category) {
        throw Error(
            'note_id, analyzer_name or category missing from properties: ' +
          JSON.stringify(comment.properties));
      }
      const request = {
        note_id: comment.properties.note_id,
        analyzer_name: comment.properties.analyzer_name,
        category: comment.properties.category,
        account_info: {
          username: accountInfo.username,
          email: accountInfo.email,
        },
      };
      const response = await this._reportNotUseful(request);
      response.text().then(text => {
        if (this._isOk(response)) {
          return this._parseRpcJson(text);
        }
        throw Error(`HTTP status: ${response.status}\nBody: ${text}`);
      });
    }

    /** Checks if the response indicates an OK response. */
    _isOk(response) {
      return response.status == 200;
    }

    /** @return {any} Resolves to parsed object. */
    _parseRpcJson(text) {
      const jsonPrefix = ')]}\'';
      return JSON.parse(text.substring(jsonPrefix.length));
    }

    /**
     * Make a reportNotUseful request with JSON data to the given service.
     *
     *
     * @param {Object} request Request message object.
     * @return {Promise} Resolves to a fetch Response object.
     */
    async _reportNotUseful(request) {
      const headers = new Headers({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      });
      const opts = {
        headers,
        method: 'POST',
        body: JSON.stringify(request),
      };
      return await fetch(this.url, opts);
    }
  }

  window.mean = window.mean || {};
  Object.assign(window.mean, {
    MeanClient,
  });
})();
