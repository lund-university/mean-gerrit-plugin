package com.axis.system.gerrit.plugins.mean;

import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.extensions.restapi.RestReadView;
import com.google.gerrit.server.config.ConfigResource;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.jgit.lib.Config;

@Singleton
class GetMeanHost implements RestReadView<ConfigResource> {

  private static final String HOST_SECTION = "host";

  private final PluginConfigFactory config;
  private final String pluginName;

  @Inject
  GetMeanHost(PluginConfigFactory config, @PluginName String pluginName) {
    this.config = config;
    this.pluginName = pluginName;
  }

  @Override
  public Response<MeanHost> apply(ConfigResource configResource) {
    Config cfg = config.getGlobalPluginConfig(pluginName);
    return Response.ok(new MeanHost(cfg.getString(HOST_SECTION, null, "url")));
  }

  static class MeanHost {
    String url;

    MeanHost(String url) {
      this.url = url;
    }
  }
}
