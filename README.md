# MEAN plugin

Plugin that enables user feedback on result from static code analysis ran by
mean. It does so by adding a "not-useful"-button robot comments.


## Usage

Add "NOT USEFUL" as a highlighted text to robotcomments. If pressed and robot
comment is posted by user "svcmean", comment information is sent to host
specified in `mean.config`. Username is collected together with comment
information in order to count a not-useful comment only once per user. If the
request is successfully sent, "REPORTED NOT USEFUL" substituted for "NOT
USEFUL". If the request is not successful, "Failed to report not useful" is
presented in a black pop up box.


## Installation

Build this plugin in the gerrit directory, for more info check out "Bazel in
tree driven" at
https://gerrit-review.googlesource.com/Documentation/dev-build-plugins.html


## Configuration

The configuration file is named `mean.config` and is located on the
`meta/config` branch. Project configs use gerrits implementation of
configuration inheritance.

Sections that are used are `host`, `general` and `analyzer`.


### Host

Create a `mean.config` in branch `meta/config` for the project.
Append this to the file:

```
[host]
    mean = <url>
```

`url` is to be changed to the host where not useful reports are to be sent. A
recommendation is to set this in "All-projects" and nowhere else. This way all
"not useful"-reports end up in the same place.


### General

In the `general` section you can enable and disable the system for the project
(defaults to disabled).

```
[general]
    enable = <true|false>
```

If enabling the mean system for many projects, a recommendation is to set this
in a parent project.


### Analyzer

This gerrit configuration section gives the ability to configure analyzers
per project. An example configuration for a "shellcheck" analyzer can be
found below:

```
[analyzer "shellcheck"]
    enable = true
    timeout = 10
    regex = ^.+\.(sh|bash|ksh|dash)$
    blacklist = E001
    blacklist = E002
    blacklist = E003
```

For more information about these fields we refer to the documentation of the
mean service. A recommendation is to start with only enabling an analyzer with
default configuration like below:

```
[analyzer "shellcheck"]
    enable = true
```

## REST endpoints

The configuration adds three endpoints on the gerrit project level:

- `/meanhost`      - Returns the host used to report not useful
- `/meanenabled`   - Returns if mean is enabled or not
- `/meananalyzers` - Returns an object with all analyzer configurations in
project
