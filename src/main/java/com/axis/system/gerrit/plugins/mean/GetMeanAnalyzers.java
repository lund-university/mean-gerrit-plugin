package com.axis.system.gerrit.plugins.mean;

import com.google.common.base.Strings;
import com.google.common.flogger.FluentLogger;
import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.extensions.restapi.RestReadView;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.gerrit.server.project.NoSuchProjectException;
import com.google.gerrit.server.project.ProjectResource;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.eclipse.jgit.lib.Config;

@Singleton
class GetMeanAnalyzers implements RestReadView<ProjectResource> {
  private static final FluentLogger logger = FluentLogger.forEnclosingClass();

  private static final String ANALYZERS_SECTION = "analyzer";
  private static final String ENABLE = "enable";
  private static final String TIMEOUT = "timeout";
  private static final String REGEX = "regex";
  private static final String BLACKLIST = "blacklist";

  private final PluginConfigFactory config;
  private final String pluginName;

  @Inject
  GetMeanAnalyzers(PluginConfigFactory config, @PluginName String pluginName) {
    this.config = config;
    this.pluginName = pluginName;
  }

  @Override
  public Response<Map<String, AnalyzerConfig>> apply(ProjectResource project)
      throws NoSuchProjectException {
    Config cfg = config.getProjectPluginConfigWithInheritance(project.getNameKey(), pluginName);
    Set<String> analyzerSections = cfg.getSubsections(ANALYZERS_SECTION);
    logger.atFine().log("Project: %s, analyzer sections: %s", project.getName(), analyzerSections);

    Map<String, AnalyzerConfig> configuredAnalyzers = new HashMap<>();
    for (String analyzerSection : analyzerSections) {
      AnalyzerConfig analyzerConfig = new AnalyzerConfig();
      analyzerConfig.enable = cfg.getBoolean(ANALYZERS_SECTION, analyzerSection, ENABLE, false);
      analyzerConfig.timeout = cfg.getInt(ANALYZERS_SECTION, analyzerSection, TIMEOUT, -1);
      analyzerConfig.regex =
          Strings.nullToEmpty(cfg.getString(ANALYZERS_SECTION, analyzerSection, REGEX));
      analyzerConfig.blacklist =
          new HashSet<>(
              Arrays.asList(cfg.getStringList(ANALYZERS_SECTION, analyzerSection, BLACKLIST)));
      configuredAnalyzers.put(analyzerSection, analyzerConfig);
    }
    logger.atFine().log("Project: %s, analyzerconfig: %s", project.getName(), configuredAnalyzers);

    return Response.ok(configuredAnalyzers);
  }

  static class AnalyzerConfig {
    boolean enable;
    int timeout;
    String regex;
    Set<String> blacklist;
  }
}
