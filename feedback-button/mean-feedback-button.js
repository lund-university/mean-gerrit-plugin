(function() {
  'use strict';

  Polymer({
    is: 'mean-feedback-button',

    /**
     * Fired when the "report not useful" request fails.
     *
     * @event show-alert
     */

    properties: {
      url: String,
      accountInfo: Object,
      plugin: Object,
      comment: Object,
      disabled: {
        type: Boolean,
        value: true,
      },
      buttonText: {
        type: String,
        value: 'Not useful',
      },
    },

    /** Upon attachment in the page, initialize the element. */
    attached() {
      if (!this.comment) {
        console.warn('mean-feedback-button attached without comment');
        return;
      }
      if (!this.comment.robot_run_id) {
        return; // Not a robot comment.
      }
      // If this is not a Mean robot comment, don't show a button.
      if (!this.comment || !this.comment.author
          || this.comment.author.username !== 'svcmean') {
        return;
      }
      // Checks if flagnot_useful_pressed has been found and set to true by the MEAN system in robot_publisher.
      if (this.comment.properties.not_useful_pressed == "true") {
          // Set style.backgroundColor of entire robot comment to #bec1bb ("gray").
          this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.style.backgroundColor = "#bec1bb";
      }

      this.init();
    },

    /** Load config and set mean host name. */
    init() {
      const restApi = this.plugin.restApi();
      restApi.getLoggedIn().then(loggedIn => {
        if (loggedIn) {
          const promises = [];
          promises.push(restApi.getAccount().then(account => {
            this.accountInfo = account;
          }));
          promises.push(
              restApi.get(`/config/server/meanhost`).then(meanHost => {
                if (meanHost && meanHost.url) {
                  this.url = meanHost.url;
                  this.disabled = false;
                } else {
                  console.warn(
                      `No configured not-useful-host, cannot report feedback.`);
                }
              }));
          Promise.all(promises);
        }
      });
    },

    /** Report to the mean server that this comment was not useful. */
    reportNotUseful() {
      if (this.disabled) {
        console.warn('reportNotUseful called on disabled button');
        return Promise.reject(new Error('unexpected reportNotUseful call'));
      }
      this.disabled = true;
      const client = new window.mean.MeanClient(this.url);
      client.reportNotUseful(this.comment, this.accountInfo)
          .then(() => {
            this.buttonText = 'Reported not useful';
          })
          .catch(err => {
            this.dispatchEvent(new CustomEvent('show-alert', {
              detail: {message: 'Failed to report not useful'},
              bubbles: true,
              composed: true,
            }));
            this.disabled = false;
            console.error(err);
          });
    },
  });
})();
