package com.axis.system.gerrit.plugins.mean;

import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.extensions.restapi.RestReadView;
import com.google.gerrit.server.config.PluginConfigFactory;
import com.google.gerrit.server.project.NoSuchProjectException;
import com.google.gerrit.server.project.ProjectResource;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.eclipse.jgit.lib.Config;

@Singleton
class GetMeanEnabled implements RestReadView<ProjectResource> {

  private static final String GENERAL_SECTION = "general";
  private static final String ENABLE = "enable";

  private final PluginConfigFactory config;
  private final String pluginName;

  @Inject
  GetMeanEnabled(PluginConfigFactory config, @PluginName String pluginName) {
    this.config = config;
    this.pluginName = pluginName;
  }

  @Override
  public Response<MeanEnabled> apply(ProjectResource project) throws NoSuchProjectException {
    Config cfg = config.getProjectPluginConfigWithInheritance(project.getNameKey(), pluginName);
    return Response.ok(new MeanEnabled(cfg.getBoolean(GENERAL_SECTION, ENABLE, false)));
  }

  static class MeanEnabled {
    boolean enable;

    MeanEnabled(boolean enable) {
      this.enable = enable;
    }
  }
}
